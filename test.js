// Soal 1
// Buat fungsi berikut menggunakan bahasa pemrograman yang paling anda kuasai (boleh js, python, php, java, dll.)
// Buat fungsi dengan menampilkan bilangan cacah kelipatan 3 atau 7 sebanyak N, serta menampilkan huruf Z saat bilangan tersebut kelipatan 3 dan 7.
// Contoh :
// N = 13
// Output : 3, 6, 7, 9, 12, 14, 15, 18, Z, 24, 27, 28, 30

console.log("Soal 1");
function MunculkanAngka(N) {
  for (let i = 1; i <= N; i++) {
    if (i % 3 === 0 || i % 7 === 0) {
      console.log(i === 21 ? "Z" : i);
    }
  }
}

// Contoh pemanggilan fungsi dengan N = 30
MunculkanAngka(30);

//   Soal 2
//   Buat fungsi pencarian ‘sang gajah’, ‘serigala’, ‘harimau’.
//   Dengan contoh masukan dan keluaran sebagai berikut :

//       Input	: Berikut adalah kisah sang gajah. Sang gajah memiliki teman serigala bernama DoeSang. Gajah sering dibela oleh serigala ketika harimau mendekati gajah.
//   Output	: sang gajah - sang gajah - serigala - serigala - harimau

console.log("\nSoal 2");
function searchAndReplace(sentence) {
  const searchWords = ["sang gajah", "serigala", "harimau"];
  const replaceWords = ["sang gajah -", "serigala -", "harimau"];

  for (let i = 0; i < searchWords.length; i++) {
    const searchWord = searchWords[i];
    const replaceWord = replaceWords[i];

    sentence = sentence.split(searchWord).join(replaceWord);
  }

  return sentence;
}

// Contoh pemanggilan fungsi
const input =
  "Berikut adalah kisah sang gajah. Sang gajah memiliki teman serigala bernama DoeSang. Gajah sering dibela oleh serigala ketika harimau mendekati gajah.";
const output = searchAndReplace(input);
console.log(output);

// Soal 3
//   Buatlah fungsi pengecekan kata sandi, dengan ketentuan sebagai
//   Kata sandi minimal 8 karakter
//   Kata sandi maksimal 32 karakter
//   Karakter awal tidak boleh angka
//   Harus memiliki angka
//   Harus memiliki huruf kapital dan huruf kecil

console.log("\nSoal 3");
function cekKataSandi(kataSandi) {
  if (/^\d/.test(kataSandi)) {
    return "Karakter awal tidak boleh angka";
  }

  if (!/[a-z]/.test(kataSandi) || !/[A-Z]/.test(kataSandi)) {
    return "Harus memiliki huruf kapital dan huruf kecil";
  }

  if (kataSandi.length < 8 || kataSandi.length > 32) {
    return "Kata sandi harus memiliki panjang antara 8 dan 32 karakter";
  }

  return "Kata sandi valid";
}

// Contoh pemanggilan fungsi
console.log(cekKataSandi("sandiwara4")); // Output: Harus memiliki huruf kapital dan huruf kecil
console.log(cekKataSandi("Sandiwara4")); // Output: Kata sandi valid
console.log(cekKataSandi("Sandiwar4")); // Output: Karakter awal tidak boleh angka


// Soal 4 
// Buat fungsi pengecekan bilangan cacah terkecil yang tidak ada dari data
// yang diinputkan. Dengan contoh input dan output sebagai berikut :

console.log("\nSoal 4");
function cariBilanganCacahTerkecil(arr) {
  let smallestPositive = 1;

  while (arr.includes(smallestPositive)) {
    smallestPositive++;
  }

  return smallestPositive;
}
const input1 = [5, 2, 8, 4, 3, 10];
const output1 = cariBilanganCacahTerkecil(input1);
console.log("Contoh 1:");
console.log("Input:", input1);
console.log("Output:", output1);

// Contoh 2
const input2 = [2, 3, 4, 6];
const output2 = cariBilanganCacahTerkecil(input2);
console.log("Contoh 2:");
console.log("Input:", input2);
console.log("Output:", output2);

// Contoh 3
const input3 = [8, 6, 7, 12];
const output3 = cariBilanganCacahTerkecil(input3);
console.log("Contoh 3:");
console.log("Input:", input3);
console.log("Output:", output3);

// Soal 5 
// Buat pola berikut sesuai inputan N, dengan N adalah bilangan ganjil

console.log("\nSoal 5");
function createPattern(N) {
  if (N % 2 === 0) {
    console.log("N harus merupakan bilangan ganjil.");
    return;
  }

  let pattern = "";
  const middleIndex = Math.floor(N / 2);

  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N; j++) {
      if (
        i === 0 ||
        i === N - 1 ||
        j === 0 ||
        j === N - 1 ||
        i === middleIndex ||
        j === middleIndex
      ) {
        pattern += "*";
      } else {
        pattern += " ";
      }
    }
    pattern += "\n";
  }

  console.log(pattern);
}

// Contoh penggunaan
createPattern(5);
createPattern(7);
createPattern(9);
